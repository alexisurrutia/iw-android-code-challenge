# Pokemon List

Pokemon List, muestra un listado de 20 items de distintos Pokemon, con su respectiva imagen y nombre, los datos son proporcionados por una Api facilitada por la empresa para la realización de la Prueba Técnica.

Al presionar cada item, se realiza una acción que enlaza a otra ventana la que contiene informacion relacionada con el Pokemon seleccionado, esta ventana esta compuesta por la siguiente información: Imagen del Pokemon, nombre, numero, generación, especie y sus habilidades.




### Pre-requisitos 📋

_Herramienta necesaria para instalar el software

```
Celular
```

### Instalación 🔧

Para instalar y correr el proyecto, solo se necesita ejecutar.



## Construido con 🛠️

* IDE:   Android Studio
* LENGUAJE: JAVA
* Librerias:
  * Glide
  * Volley



## Versión 📌

1.0



## Autor ✒️



* **Jhony Alexis Urrutia Fuentes** 

  
