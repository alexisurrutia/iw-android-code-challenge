package com.example.pruebaidaworksjhonyurrutia.adaptador;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.pruebaidaworksjhonyurrutia.R;
import com.example.pruebaidaworksjhonyurrutia.clases.Pokemon;


import java.util.ArrayList;
import java.util.List;

public class PokemonAdapter extends RecyclerView.Adapter<PokemonAdapter.MyViewHolder> {

    RequestOptions options ;
    private Context mContext ;
    private ArrayList<Pokemon> mData ;
    private OnItemClickListener mListener;
    private int resourceLayout;

    public interface OnItemClickListener{

        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        mListener = listener;
    }

    public PokemonAdapter(Context mContext, ArrayList<Pokemon> lst, int resourse) {


        this.mContext = mContext;
        this.mData = lst;
        this.resourceLayout = resourse;
        options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.background_splash)
                .error(R.drawable.background_splash);

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view ;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(resourceLayout,parent,false);
        // click listener here
        return new MyViewHolder(view);
       // return null;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        Pokemon item = mData.get(position);

        String imageUrl = item.getUrlImagen();
        String imageName = item.getNombre();
        String rutaPokemon = item.getRutaPokemon();

        holder.tv_name.setText(imageName);
        holder.tv_ruta_pokemon.setText(rutaPokemon);

        // load image from the internet using Glide
        Glide.with(mContext).load(imageUrl).apply(options).into(holder.iv_url);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tv_name, tv_ruta_pokemon;
        ImageView iv_url;


        public MyViewHolder(View itemView){
            super(itemView);

            tv_name = itemView.findViewById(R.id.rowname);
            iv_url = itemView.findViewById(R.id.iv_urlImage);
            tv_ruta_pokemon = itemView.findViewById(R.id.tv_ruta_pokemon);

            itemView.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    if(mListener!=null){
                        int position = getAdapterPosition();

                        if(position != RecyclerView.NO_POSITION){
                            mListener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }

}
