package com.example.pruebaidaworksjhonyurrutia;


import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.pruebaidaworksjhonyurrutia.adaptador.PokemonAdapter;
import com.example.pruebaidaworksjhonyurrutia.clases.Pokemon;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

public class MainActivity extends AppCompatActivity implements PokemonAdapter.OnItemClickListener {

    public static final String EXTRA_URL = "imageUrl";
    public static final String EXTRA_NAME = "namePokemon";
    public static final String EXTRA_RUTA_POKEMON = "rutaPokemon";
    public  static String retornaUrl = "";
    String param1, param2;
    ImageView imgView;
    LinearLayout errorConexion;
    Button btn_reconectar;
    EditText tv_url_add;


    private RecyclerView mRecyclerView ;

    private JsonArrayRequest ArrayRequest ;
    private RequestQueue mRequestQueue ;
    private ArrayList<Pokemon> lstPokemon;
    private PokemonAdapter mPokemonAdapter;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //imgView = findViewById(R.id.imageView);
        //errorConexion = findViewById(R.id.errorConexion);
        //btn_reconectar = findViewById(R.id.btn_reconectar);

        mRecyclerView = findViewById(R.id.rv_principal);
       // mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        LinearLayoutManager manager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setHasFixedSize(true);

        tv_url_add = findViewById(R.id.tv_url_add);

        //mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mRecyclerView.setLayoutManager(new GridLayoutManager(this,2));

        lstPokemon= new ArrayList<>();

        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setTitle("Procesando");
        progressDialog.setMessage("Porfavor espere...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        jsoncall();

    }


    public void jsoncall() {

        String URL_JSON = "https://pokeapi.co/api/v2/pokemon/";

        final JsonObjectRequest ArrayRequest = new JsonObjectRequest(Request.Method.GET, URL_JSON, null, new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {

                JSONObject hit = null;
                //JSONObject objectItem = null;

                try {
                    JSONArray jsonArray = response.getJSONArray("results");
                    //Toast.makeText(MainActivity.this,"PASO 2",Toast.LENGTH_LONG).show();

                    String imagen = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/";


                    for (int i = 0 ; i<jsonArray.length();i++) {

                        hit = jsonArray.getJSONObject(i);

                        String nombre       = hit.getString("name");
                        String ruta_pokemon = hit.getString("url");
                        //Log.i("URRUTIA","->"+valor);

                       int count = i+1;

                        lstPokemon.add(new Pokemon(nombre,imagen+count+".png",ruta_pokemon));

                    }


                    mPokemonAdapter = new PokemonAdapter(getApplicationContext(),lstPokemon,R.layout.pokemon_row_item);
                    mRecyclerView.setAdapter(mPokemonAdapter);
                    mPokemonAdapter.setOnItemClickListener(MainActivity.this);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

                if (error instanceof NetworkError) {
                    progressDialog.setTitle("Error de conexion");
                    progressDialog.setMessage("Porfavor verifique si tiene acceso a internet!");
                }
            }
        });
        mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        mRequestQueue.add(ArrayRequest);


    }



    public String getURL2(String url){
        final String[] retornourl = {null};
       StringRequest peticion = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String respuesta) {

                        try {
                            //tv_url_add.setText("");
                            String obj_data = null;
                            JSONObject arrayData = new JSONObject(respuesta);

                            obj_data = arrayData.getString("sprites");
                            JSONObject arrayData2 = new JSONObject(obj_data);

                            Log.i("JHONY-2",arrayData2.getString("back_default"));

                            tv_url_add.setText(arrayData2.getString("back_default"));
                            retornourl[0] = arrayData2.getString("back_default");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }
        );
        Volley.newRequestQueue(MainActivity.this).add(peticion);

        //Toast.makeText(MainActivity.this,"PASO  - "+retornaUrl,Toast.LENGTH_LONG).show();
        return retornourl[0];
    }



    @Override
    public void onItemClick(int position) {

        Funciones funcion = new Funciones();

        if (funcion.compruebaConexion(MainActivity.this)) {

            Intent detalleIntent = new Intent(this, DetalleActivity.class);
            Pokemon clickedItem = lstPokemon.get(position);

            detalleIntent.putExtra(EXTRA_URL, clickedItem.getUrlImagen());
            detalleIntent.putExtra(EXTRA_NAME, clickedItem.getNombre());
            detalleIntent.putExtra(EXTRA_RUTA_POKEMON,clickedItem.getRutaPokemon());

            startActivity(detalleIntent);
        }else{
            Toast.makeText(MainActivity.this,"Error de conexión!, porfavor verifique si tiene acceso a internet!",Toast.LENGTH_LONG).show();
        }
    }


}