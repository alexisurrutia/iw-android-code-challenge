package com.example.pruebaidaworksjhonyurrutia.clases;

public class Pokemon {

    public int posicion;
    public String nombre;
    public String urlImagen;
    public String rutaPokemon;

    public String getRutaPokemon() {
        return rutaPokemon;
    }

    public void setRutaPokemon(String rutaPokemon) {
        this.rutaPokemon = rutaPokemon;
    }

    public Pokemon() {

    }
    public Pokemon( String nombre, String urlImagen, String rutaPokemon) {
       // this.posicion = posicion;
        this.nombre = nombre;
        this.urlImagen = urlImagen;
        this.rutaPokemon = rutaPokemon;
    }

    public int getPosicion() {
        return posicion;
    }

    public void setPosicion(int posicion) {
        this.posicion = posicion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUrlImagen() {
        return urlImagen;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }
}
