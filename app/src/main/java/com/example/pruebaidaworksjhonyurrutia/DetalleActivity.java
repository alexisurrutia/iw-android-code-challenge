package com.example.pruebaidaworksjhonyurrutia;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.example.pruebaidaworksjhonyurrutia.adaptador.PokemonAdapter;
import com.example.pruebaidaworksjhonyurrutia.clases.Pokemon;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;

import static com.example.pruebaidaworksjhonyurrutia.MainActivity.EXTRA_NAME;
import static com.example.pruebaidaworksjhonyurrutia.MainActivity.EXTRA_RUTA_POKEMON;
import static com.example.pruebaidaworksjhonyurrutia.MainActivity.EXTRA_URL;

public class DetalleActivity extends AppCompatActivity {

    TextView text_nombre;
    ImageView imageView;

    private RequestQueue mRequestQueue ;
    private ArrayList<Pokemon> lstPokemon;
    private PokemonAdapter mPokemonAdapter;
    private RecyclerView mRecyclerView ;
    ProgressDialog progressDialog;
    Button btn_weight, btn_generacion;
    TextView text_species,text_numero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle);

        Intent intent = getIntent();
        String imageUrl = intent.getStringExtra(EXTRA_URL);
        String nombrePokemon = intent.getStringExtra(EXTRA_NAME);
        String rutaPokemon = intent.getStringExtra(EXTRA_RUTA_POKEMON);

        mRecyclerView = findViewById(R.id.rv_detalle);


        LinearLayoutManager manager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        lstPokemon= new ArrayList<>();


        imageView = findViewById(R.id.imageView_detail);
        text_nombre = findViewById(R.id.text_name);

        btn_weight     = findViewById(R.id.btn_weight);
        btn_generacion = findViewById(R.id.btn_generacion);

        text_numero = findViewById(R.id.text_numero);
        text_species = findViewById(R.id.text_species);

        // load image from the internet using Glide
        Glide.with(this).load(imageUrl).placeholder(R.drawable.background_splash).error(R.drawable.background_splash).into(imageView);

        text_nombre.setText(nombrePokemon);

        progressDialog = new ProgressDialog(DetalleActivity.this);
        progressDialog.setTitle("Procesando");
        progressDialog.setMessage("Porfavor espere...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        //Toast.makeText(DetalleActivity.this,"PROBANDO...",Toast.LENGTH_SHORT).show();
        jsoncall(rutaPokemon);

    }

    public void jsoncall(String url){

        String URL_JSON = url;

        final JsonObjectRequest ArrayRequest = new JsonObjectRequest(Request.Method.GET, URL_JSON, null, new Response.Listener<JSONObject>() {
            //      ArrayRequest = new JsonArrayRequest(URL_JSON, new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONObject response) {

                JSONObject hit = null;
                JSONObject specie = null;
                try {
                    JSONArray jsonArray = response.getJSONArray("abilities");//Accediendo a array
                    JSONObject jsonSpecieArray = response.getJSONObject("species");//Accediendo a objeto
                    JSONObject jsonSpritesArray = response.getJSONObject("sprites");//Accediendo a objeto

                    for (int i = 0 ; i<jsonArray.length();i++) {

                        hit     = jsonArray.getJSONObject(i);

                        JSONObject obj = hit.getJSONObject("ability");

                        String nombre = obj.getString("name");
                        String imageUrl = jsonSpecieArray.getString("url");//
                        String rutaPokemon = jsonSpecieArray.getString("url");//url_species

                        lstPokemon.add(new Pokemon(nombre,imageUrl,rutaPokemon));
                       // Toast.makeText(DetalleActivity.this,nombre+" // "+imageUrl,Toast.LENGTH_SHORT).show();
                    }

                    // load image from the internet using Glide
                    Glide.with(DetalleActivity.this).load(jsonSpritesArray.getString("back_default")).placeholder(R.drawable.background_splash).error(R.drawable.background_splash).into(imageView);

                    mPokemonAdapter = new PokemonAdapter(getApplicationContext(),lstPokemon,R.layout.pokemon_ability_row_item);
                    mRecyclerView.setAdapter(mPokemonAdapter);

                    //seteando WEIGHT
                    btn_weight.setText("Weight:  "+response.getString("weight"));

                    //Volley
                    obtnerSpecies(jsonSpecieArray.getString("url"));



                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

                if (error instanceof NetworkError) {
                    progressDialog.setTitle("Error de conexion");
                    progressDialog.setMessage("Porfavor verifique si tiene acceso a internet!");
                }
            }
        });
        mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        mRequestQueue.add(ArrayRequest);
    }

    public void obtnerSpecies(String url){

        String URL_JSON = url;

        final JsonObjectRequest ArrayRequest = new JsonObjectRequest(Request.Method.GET, URL_JSON, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {


                try {
                    JSONArray jsonArray1 = response.getJSONArray("egg_groups");//Accediendo a array
                    JSONObject jsonArrayGen = response.getJSONObject("generation");//Accediendo a array

                    for (int i = 0 ; i<jsonArray1.length();i++) {


                        String especie_name = jsonArray1.getJSONObject(i).getString("name");

                        text_species.setText(especie_name);

                        //Toast.makeText(DetalleActivity.this,especie_name,Toast.LENGTH_SHORT).show();
                    }

                    //SETEANDO...
                    //BTN GENERACION
                    btn_generacion.setText(jsonArrayGen.getString("name"));

                    //TEXT ID
                    text_numero.setText("#:  "+response.getString("id"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

                if (error instanceof NetworkError) {
                    progressDialog.setTitle("Error de conexion");
                    progressDialog.setMessage("Porfavor verifique si tiene acceso a internet!");
                }
            }
        });
        mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        mRequestQueue.add(ArrayRequest);
    }


}
